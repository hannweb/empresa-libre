# Empresa Libre

## Mini ERP para la legislación mexicana


**En cada relación comercial, hay una relación humana**


Este proyecto está en continuo desarrollo, contratar un esquema de soporte,
nos ayuda a continuar su desarrollo. Ponte en contacto con nosotros para
contratar: administracion ARROBA empresalibre.net

#### Ahora también puede aportar con Bitcoin Cash (BCH):

`pq763fj7kxxf2wtf360lfsy5ydw84yz72q76hanhxq`


### Requerimientos:

* Servidor web, recomendado Nginx
* uwsgi
* python3.6+
* xsltproc
* openssl
* xmlsec

Debería de funcionar con cualquier combinación servidor-wsgi que soporte
aplicaciones Python.

El sistema tiene soporte solo para PostgreSQL, debes de instalar el servidor de
la base de datos y su driver respectivo.



